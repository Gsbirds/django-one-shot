from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList
from .forms import TodoForm, TodoItem, ItemForm

# Create your views here.


def show_list(request):
    lists= TodoList.objects.all()
    context= {
        "lists":lists,
    }
    return render(request, "todos/list.html", context)

def show_detail(request, id):
    todo_list=get_object_or_404(TodoList, id=id)
    context={
        "list_object": todo_list,
    }
    return render(request, "todos/detail.html", context)

def create_list(request):
    if request.method=="POST":
        form= TodoForm(request.POST)
        if form.is_valid():
            form=form.save()
            return redirect("todo_list_detail", id=form.id)
    else:
        form=TodoForm()
    context={
        "form":form
    }
    return render(request, "todos/create.html", context)

def edit_list(request, id):
    post= get_object_or_404(TodoList, id=id)
    if request.method=="POST":
        form= TodoForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=post.id)
    else:
        form=TodoForm(instance=post)
    context={
        "post_object":post,
        "post_form":form,
    }

    return render(request, "todos/edit.html", context)


def delete_list(request, id):
  model_instance = TodoList.objects.get(id=id)
  if request.method == "POST":
    model_instance.delete()
    return redirect("todo_list_list")

  return render(request, "todos/delete.html")

def create_item(request):
    if request.method=="POST":
        form= ItemForm(request.POST)
        if form.is_valid():
            form=form.save()
            return redirect("todo_list_detail", id=form.list.id)
    else:
        form=ItemForm()
    context={
        "form":form
    }
    return render(request, "todos/create_item.html", context)

def update_item(request, id):
    post= get_object_or_404(TodoItem, id=id)
    if request.method=="POST":
        form= ItemForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=post.list.id)
    else:
        form=ItemForm(instance=post)
    context={
        "post_object":post,
        "post_form":form,
    }

    return render(request, "todos/update.html", context)
