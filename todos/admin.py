from django.contrib import admin
from todos.models import TodoList, TodoItem

# Register your models here.
class TodoListAdmin(admin.ModelAdmin):
    list_display=(
        "name",
        "created_on",
        "id",
    )
admin.site.register(TodoList, TodoListAdmin)

class TodoItemAdmin(admin.ModelAdmin):
    list_display=(
        "task",
        "due_date",
        "is_completed",
        "id",
    )
admin.site.register(TodoItem, TodoItemAdmin)